<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kansio_Model extends CI_Model {
        public function __construct() {
                parent::__construct();
                $this->load->helper('directory');
        }
        
        public function hae_kaikki() {
            return array_keys(directory_map($this->config->item("upload_path")));
        }
        
        public function lisaa($kansio) {
            if (!mkdir($this->config->item('upload_path') . $kansio)) {
                throw new Expection("Virhe luotaessa kansiota. Kansion luominen ei onnistunut.");
            }
        }
        
        public function poista($kansio) {
            if (!rmdir($this->config->item('upload_path') . $kansio)) {
                throw new Expection("Kansion poistaminen ei onnistu. Tarkasta, että kansiossa ei ole kuvia.");
            }
        }
    
        /* Tässä tapauksessa tietoja ei tallenneta tietokantaan, vaan 
         * malli käsittelee kansioita tiedostojärjestelmässä */
        
}