<!-- Kansiot_view-näkymässä tulostetaan käsittelijässä asetettu kansiot-muuttujan
sisältö (taulukko kansion nimiä). Aluksi tulostetaan kuva (kiinni olevasta kansioista)
sekä kansion nimi linkkinä. Kuvat_view on tyhjä, joten vaikka näkymä ladataan, ei
sen perusteella tulosteta sivulle mitään.-->

<?php
foreach ($kansiot as $kansio) {
    $ominaisuudet="";
    $kuva='img/kansio_kiinni.PNG';
    
    // Poistetaan kauttaviiva kansion nimen lopusta.
    $kansio=substr($kansio, 0, strlen($kansio)-1);
    
    if ($kansio==$valittu) {
        $kuva='img/kansio_auki.PNG';
        $ominaisuudet=array('class' => 'valittu_kansio');
    }
        
    echo img($kuva);
    echo anchor('galleria/index/' . urlencode($kansio),$kansio,$ominaisuudet);
    echo br();
}